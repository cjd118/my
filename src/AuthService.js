import auth0 from 'auth0-js';
import EventEmitter from 'eventemitter3';
// import { AUTH_CONFIG } from './auth0-variables'

export default class AuthService {

  auth0 = new auth0.WebAuth({
    domain: 'clientlogin.eu.auth0.com',
    clientID: 'mTwjt9XuW2O715QnJFcarY7NdJLoRcLe',
    redirectUri: 'http://vm:8080/callback',
    responseType: 'token id_token',
    scope: 'openid'
  })

  authNotifier = new EventEmitter()

  handleAuthentication (cb) {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult, cb);
      } else if (err) {
        console.log(err)
      }
    })
  }

  setSession (authResult, cb) {
    // Set the time that the Access Token will expire at
    let expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    )
    localStorage.setItem('access_token', authResult.accessToken)
    localStorage.setItem('id_token', authResult.idToken)
    localStorage.setItem('expires_at', expiresAt)
    this.authNotifier.emit('authChange', { authenticated: true })
    cb();
  }

  login () {
    this.auth0.authorize()
  }

  logout (cb) {
    // Clear Access Token and ID Token from local storage
    localStorage.removeItem('access_token')
    localStorage.removeItem('id_token')
    localStorage.removeItem('expires_at')
    this.authNotifier.emit('authChange', false)
    cb();
  }


  isAuthenticated () {
    // Check whether the current time is past the
    // Access Token's expiry time
    let expiresAt = JSON.parse(localStorage.getItem('expires_at'))
    return new Date().getTime() < expiresAt
  }  
}