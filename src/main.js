import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'bootstrap/scss/bootstrap.scss'
import AuthService from './AuthService'
import Default from './layouts/Default.vue'

Vue.config.productionTip = false

Vue.component('default', Default)

Object.defineProperty(Vue.prototype, '$auth', { value: new AuthService() });

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
