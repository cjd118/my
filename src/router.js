import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Login from './views/Login.vue';
import Logout from './views/Logout.vue';
import Callback from './views/Callback.vue';
import Services from './views/Services.vue';

Vue.use(Router)

var router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout
    },
    {
      path: '/callback',
      name: 'callback',
      component: Callback
    },
    {
      path: '/services',
      name: 'services',
      component: Services,
      meta: {
        guest: false
      }
    }
  ]
})

router.beforeEach((to,from,next) => {
  if(to.matched.some(record => record.meta.guest === false)){
    if(!router.app.$auth.isAuthenticated()){
      next('/login')
    }
  }
  next();
});

export default router
